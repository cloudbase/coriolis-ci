@Library('common-lib') _

currentBuild.displayName = "#${currentBuild.number}-${params.CONFIG_FILE_CREDS_ID}"

CI = new coriolis.ci.Appliance()

PROVIDER = CI.get_appliance_providers()

PROVIDER_CONN_INFO = PROVIDER[params.PLATFORM].connection_info

def tests_ids = readJSON (text: params.TESTS_IDS)
CI.validate_config(params.CONFIG_FILE_CREDS_ID, tests_ids)

def config = CI.get_config(params.CONFIG_FILE_CREDS_ID)

def ssh_info = null
def coriolis_api_creds_info = null
try {
    ssh_info = CI.get_appliance_default_ssh_info(params.APPLIANCE_IP)
    ssh_info.user = params.APPLIANCE_SSH_USER
    ssh_info.password = env.APPLIANCE_SSH_USER_PASS

    coriolis_api_creds_info = CI.get_coriolis_api_creds_info(ssh_info)
    CI.expose_coriolis(ssh_info)
    CI.download_appliance_certificate(params.APPLIANCE_IP)
    CI.add_coriolis_licence(ssh_info, coriolis_api_creds_info)
    CI.add_coriolis_endpoints(config, coriolis_api_creds_info)

    def testing_stages = [:]
    def test_count = 1
    for (test in config['tests']) {
        if (!(test.id in tests_ids)) {
            continue
        }
        def test_index = test_count
        def current_test = CI.deep_copy_hashmap(test)
        def export_provider = CI.deep_copy_hashmap(config['export_providers'].find { p -> p.id == test.export_provider_id })
        def import_provider = CI.deep_copy_hashmap(config['import_providers'].find { p -> p.id == test.import_provider_id })
        def export_endpoint = CI.deep_copy_hashmap(config['endpoints'].find { p -> p.name == export_provider.endpoint_name })
        testing_stages["#${test_count} ${test.id} ${test.scenario}: ${export_provider.id} to ${import_provider.id}"] = {
            CI.run_test(params.PLATFORM, test_index, current_test, export_endpoint, export_provider, import_provider,
            coriolis_api_creds_info, ssh_info, params.TESTS_RESOURCES_CLEANUP, PROVIDER_CONN_INFO)
        }
        test_count = test_count + 1
    }
    parallel testing_stages
} finally {
    if ((ssh_info != null) && (coriolis_api_creds_info != null)) {
        CI.collect_logs(ssh_info, coriolis_api_creds_info)
    }
}


