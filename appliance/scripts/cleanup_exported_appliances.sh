#!/usr/bin/env bash
set -o errexit
set -o pipefail

APPLIANCE_EXPORT_DIR="/home/ubuntu/nginx-root/appliances"

find $APPLIANCE_EXPORT_DIR -type f -mtime +7 -name coriolis-appliance-jenkins-ci-* | grep -v "$(readlink $APPLIANCE_EXPORT_DIR/coriolis-appliance-latest{.ova,.ova.sha256sum,.passwd})" | xargs rm -f
