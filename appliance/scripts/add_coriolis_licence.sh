#!/usr/bin/env bash
set -o errexit
set -o pipefail

if [[ -z $APPLIANCE_IP ]]; then echo "ERROR: The env variable APPLIANCE_IP is not set"; exit 1; fi
if [[ -z $GENERATOR_BIN_URL ]]; then echo "ERROR: The env variable GENERATOR_BIN_URL is not set"; exit 1; fi

export TIMEOUT=1800

cd ~

echo "Downloading licensing generator binary"
SECONDS=0
while [[ $SECONDS -lt $TIMEOUT ]]; do
    curl --silent $GENERATOR_BIN_URL -o generator && break || sleep 10
done

chmod +x generator

SECONDS=0
while [[ $SECONDS -lt $TIMEOUT ]]; do
    APPLIANCE_ID=$(coriolis licensing appliance list -c "ID" -f value | awk '{print $1}') \
        && break || sleep 10
done

echo "Appliance ID: $APPLIANCE_ID"

END_DATE=`date -d '+1 month' --iso-8601=seconds`
SECONDS=0
LICENSE_FILE_NAME=license`date +%M%S`.pem
while [[ $SECONDS -lt $TIMEOUT ]]; do
    echo "Trying to apply a new Coriolis licence"
    rm -f $LICENSE_FILE_NAME
    ./generator generate --appliance-id $APPLIANCE_ID --output-file $LICENSE_FILE_NAME \
                         -e $END_DATE -d 20 --migrations 100 --replicas 100
    coriolis licensing licence register --licence-pem-file $LICENSE_FILE_NAME $APPLIANCE_ID && break || sleep 10
done
