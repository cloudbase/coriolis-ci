#!/usr/bin/env bash
set -e

if [[ $# -ne 1 ]]; then
    echo "USAGE: $0 <nginx_root_directory>"
    exit 1
fi

CONTAINER_NAME="jenkins_proxy"

if [[ "$(docker ps -aq -f name=$CONTAINER_NAME)" != "" ]]; then
    echo "Jenkins proxy container already exists"
    exit 0
fi

DIR=$(cd $(dirname $0) && pwd)

if [[ -e "$DIR/ssl" ]]; then
    rm -rf "$DIR/ssl"
fi

mkdir -p "$DIR/ssl"
openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 \
            -subj "/C=RO/ST=Timis/L=Timisoara/CN=Coriolis Self Signed" \
            -keyout "$DIR/ssl/coriolis.key" -out "$DIR/ssl/coriolis.crt"

sudo docker run -v "$1:/usr/share/nginx/html:ro" \
                -v "$DIR/conf.d:/etc/nginx/conf.d:ro" \
                -v "$DIR/ssl:/etc/nginx/ssl:ro" \
                --detach --restart unless-stopped \
                --network host \
                --name $CONTAINER_NAME \
                nginx:latest
