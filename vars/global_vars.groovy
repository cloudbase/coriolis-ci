import groovy.transform.Field

@Field JENKINS_SLAVES_LABEL = "coriolis-ci"
@Field JENKINS_GLOBAL_TIMEOUT = 300 // value in minutes

@Field CORIOLIS_DOCKER_REPO_URL = "https://bitbucket.org/cloudbase/coriolis-docker"
@Field CORIOLIS_DOCKER_REPO_BRANCH = "master"

@Field DOCKER_REGISTRY = "https://registry.cloudbase.it"
@Field DOCKER_CORIOLIS_TOOLS_IMAGE = "registry.cloudbase.it/coriolis-tools:2412.1"

@Field DOCKER_REGISTRY_CREDS_ID = "docker-creds"
@Field APPLIANCE_CREDS_ID = "appliance-creds"
@Field REPO_SSH_KEY_CREDS_ID = "repo-ssh-key"
@Field VSPHERE_CONN_INFO_CREDS_ID = "vsphere-conn-info"
@Field VSPHERE_ENV_INFO_CREDS_ID = "vsphere-env-info"
@Field LICENSING_GENERATOR_BIN_CREDS_ID = "licensing-generator-bin-url"

@Field RELEASE_TAG = "latest"

@Field LOGS_DIR = "/home/cloudbase/SANStorage/coriolis-ci/logs"
@Field LOGS_BASE_URL = "http://coriolis-ci/logs"

@Field APPLIANCE_EXPORT_DIR = "/home/cloudbase/SANStorage/coriolis-ci/appliances"
@Field APPLIANCE_BASE_URL = "http://coriolis-ci/appliances"

@Field SUPPORTED_PUBLIC_CLOUDS = ["azure", "aws", "metal"]

@Field APPLIANCE_KEYSTONE_PORT = 5000
